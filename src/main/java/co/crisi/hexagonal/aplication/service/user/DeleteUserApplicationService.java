package co.crisi.hexagonal.aplication.service.user;

import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.service.user.DeleteUserService;
import org.springframework.stereotype.Component;

@Component
public class DeleteUserApplicationService {

    private final DeleteUserService deleteUserService;

    public DeleteUserApplicationService(DeleteUserService deleteUserService) {
        this.deleteUserService = deleteUserService;
    }

    public void run(String userId) {
        this.deleteUserService.delete(userId);
    }
}
