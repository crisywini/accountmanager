package co.crisi.hexagonal.aplication.service.account;

import co.crisi.hexagonal.domain.dto.AccountDto;
import co.crisi.hexagonal.domain.service.account.GetAllAccountsService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetAllAccountsApplicationService {
    private final GetAllAccountsService getAllAccountsService;

    public GetAllAccountsApplicationService(GetAllAccountsService getAllAccountsService) {
        this.getAllAccountsService = getAllAccountsService;
    }

    public List<AccountDto> run() {
        return getAllAccountsService.getAll();
    }
}
