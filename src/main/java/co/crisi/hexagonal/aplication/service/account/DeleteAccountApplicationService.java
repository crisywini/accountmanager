package co.crisi.hexagonal.aplication.service.account;

import co.crisi.hexagonal.domain.service.account.DeleteAccountService;
import org.springframework.stereotype.Component;

@Component
public class DeleteAccountApplicationService {
    private final DeleteAccountService deleteAccountService;

    public DeleteAccountApplicationService(DeleteAccountService deleteAccountService) {
        this.deleteAccountService = deleteAccountService;
    }

    public void run(String userName, String domain) {
        deleteAccountService.delete(userName, domain);
    }
}
