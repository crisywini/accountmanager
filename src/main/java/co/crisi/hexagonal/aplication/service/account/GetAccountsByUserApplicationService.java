package co.crisi.hexagonal.aplication.service.account;

import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.service.account.GetAccountsByUserService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetAccountsByUserApplicationService {
    private final GetAccountsByUserService getAccountsByUserService;

    public GetAccountsByUserApplicationService(GetAccountsByUserService getAccountsByUserService) {
        this.getAccountsByUserService = getAccountsByUserService;
    }

    public List<Account> run(String userName) {
        return getAccountsByUserService.getByUser(userName);
    }
}
