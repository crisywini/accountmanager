package co.crisi.hexagonal.aplication.service.account;

import co.crisi.hexagonal.domain.model.Category;
import co.crisi.hexagonal.domain.service.account.SaveAccountService;
import org.springframework.stereotype.Component;

@Component
public class SaveAccountApplicationService {
    private final SaveAccountService saveAccountService;

    public SaveAccountApplicationService(SaveAccountService saveAccountService) {
        this.saveAccountService = saveAccountService;
    }

    public void run(String userName, String domain, String key, Category category) {
        saveAccountService.save(userName, domain, key, category);
    }

}
