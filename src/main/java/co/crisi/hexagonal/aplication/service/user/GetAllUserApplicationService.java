package co.crisi.hexagonal.aplication.service.user;

import co.crisi.hexagonal.domain.dto.UserDto;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.service.user.GetAllUsersService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetAllUserApplicationService {

    private final GetAllUsersService getAllUsersService;

    public GetAllUserApplicationService(GetAllUsersService getAllUsersService) {
        this.getAllUsersService = getAllUsersService;
    }


    public List<UserDto> run() {
        return this.getAllUsersService.getAll();
    }
}
