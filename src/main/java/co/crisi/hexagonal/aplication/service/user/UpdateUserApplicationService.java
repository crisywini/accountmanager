package co.crisi.hexagonal.aplication.service.user;

import co.crisi.hexagonal.domain.exception.EntityNullException;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import co.crisi.hexagonal.domain.service.user.UpdateUserService;
import org.springframework.stereotype.Component;

@Component
public class UpdateUserApplicationService {
    private final UpdateUserService updateUserService;

    public UpdateUserApplicationService(UpdateUserService updateUserService) {
        this.updateUserService = updateUserService;
    }

    public void run(User user) {
        this.updateUserService.update(user);
    }

}
