package co.crisi.hexagonal.aplication.service.user;

import co.crisi.hexagonal.domain.exception.RepeatedEntityException;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import co.crisi.hexagonal.domain.service.user.SaveUserService;
import org.springframework.stereotype.Component;

@Component
public class SaveUserApplicationService {
    private final SaveUserService saveUserService;

    public SaveUserApplicationService(SaveUserService saveUserService) {
        this.saveUserService = saveUserService;
    }

    public void run(String userId, String password) {
        saveUserService.save(new User(userId, password));
    }

}
