package co.crisi.hexagonal.aplication.service.account;

import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.service.account.GetAccountByIdService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GetAccountByIdApplicationService {
    private final GetAccountByIdService getAccountByIdService;

    public GetAccountByIdApplicationService(GetAccountByIdService getAccountByIdService) {
        this.getAccountByIdService = getAccountByIdService;
    }

    public Optional<Account> run(String userName, String domain) {
        return getAccountByIdService.getById(userName, domain);
    }
}
