package co.crisi.hexagonal.aplication.service.user;

import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.service.user.GetUserByIdService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class GetUserByIdApplicationService {

    private final GetUserByIdService getUserByIdService;

    public GetUserByIdApplicationService(GetUserByIdService getUserByIdService) {
        this.getUserByIdService = getUserByIdService;
    }

    public Optional<User> run(String userName) {
        return this.getUserByIdService.get(userName);
    }
}
