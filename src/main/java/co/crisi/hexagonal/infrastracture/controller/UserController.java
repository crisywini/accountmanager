package co.crisi.hexagonal.infrastracture.controller;

import co.crisi.hexagonal.aplication.service.user.GetAllUserApplicationService;
import co.crisi.hexagonal.aplication.service.user.GetUserByIdApplicationService;
import co.crisi.hexagonal.aplication.service.user.DeleteUserApplicationService;
import co.crisi.hexagonal.aplication.service.user.SaveUserApplicationService;
import co.crisi.hexagonal.domain.dto.UserDto;
import co.crisi.hexagonal.domain.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(path = "api/v1/user")
public class UserController {

    private final SaveUserApplicationService saveUserApplicationService;
    private final DeleteUserApplicationService deleteUserApplicationService;
    private final GetAllUserApplicationService getAllUserApplicationService;
    private final GetUserByIdApplicationService getUserByIdApplicationService;

    public UserController(SaveUserApplicationService saveUserApplicationService,
                          DeleteUserApplicationService deleteUserApplicationService,
                          GetAllUserApplicationService getAllUserApplicationService,
                          GetUserByIdApplicationService getUserByIdApplicationService) {
        this.saveUserApplicationService = saveUserApplicationService;
        this.deleteUserApplicationService = deleteUserApplicationService;
        this.getAllUserApplicationService = getAllUserApplicationService;
        this.getUserByIdApplicationService = getUserByIdApplicationService;
    }

    @RequestMapping("/users")
    public List<UserDto> getUsers() {
        return this.getAllUserApplicationService.run();
    }


    @PostMapping
    public void save(@RequestBody Map<String, Object> json) {
        this.saveUserApplicationService.run(getValue(json, "userName"), getValue(json, "password"));
    }

    @DeleteMapping(path = "{userId}")
    public void deleteUser(@PathVariable("userId") String userId) {
        deleteUserApplicationService.run(userId);
    }

    @GetMapping(path = "{userId}")
    public Optional<User> getUserById(@PathVariable("userId") String userId) {
        return this.getUserByIdApplicationService.run(userId);
    }

    private String getValue(Map<String, Object> json, String key) {
        return json.get(key).toString();
    }
}
