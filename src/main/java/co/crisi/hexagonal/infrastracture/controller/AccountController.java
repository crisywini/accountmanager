package co.crisi.hexagonal.infrastracture.controller;

import co.crisi.hexagonal.aplication.service.account.*;
import co.crisi.hexagonal.domain.dto.AccountDto;
import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.model.Category;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "api/v1/account")
public class AccountController {
    private final SaveAccountApplicationService saveAccountApplicationService;
    private final DeleteAccountApplicationService deleteAccountApplicationService;
    private final GetAccountByIdApplicationService getAccountByIdApplicationService;
    private final GetAccountsByUserApplicationService getAccountsByUserApplicationService;
    private final GetAllAccountsApplicationService getAllAccountsApplicationService;

    public AccountController(SaveAccountApplicationService saveAccountApplicationService,
                             DeleteAccountApplicationService deleteAccountApplicationService,
                             GetAccountByIdApplicationService getAccountByIdApplicationService,
                             GetAccountsByUserApplicationService getAccountsByUserApplicationService,
                             GetAllAccountsApplicationService getAllAccountsApplicationService) {
        this.saveAccountApplicationService = saveAccountApplicationService;
        this.deleteAccountApplicationService = deleteAccountApplicationService;
        this.getAccountByIdApplicationService = getAccountByIdApplicationService;
        this.getAccountsByUserApplicationService = getAccountsByUserApplicationService;
        this.getAllAccountsApplicationService = getAllAccountsApplicationService;
    }

    @RequestMapping("/accounts")
    public List<AccountDto> getAccounts() {
        return this.getAllAccountsApplicationService.run();
    }


    @PostMapping
    public void save(@RequestBody Map<String, Object> json) {
        this.saveAccountApplicationService.run(getValue(json, "userName"),
                getValue(json, "domain"),
                getValue(json, "key"),
                Category.valueOf(getValue(json, "category")));

    }


    private String getValue(Map<String, Object> json, String key) {
        return json.get(key).toString();
    }


}
