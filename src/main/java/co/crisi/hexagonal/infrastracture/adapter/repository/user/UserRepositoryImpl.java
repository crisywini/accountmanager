package co.crisi.hexagonal.infrastracture.adapter.repository.user;

import co.crisi.hexagonal.domain.dto.UserDto;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private co.crisi.hexagonal.infrastracture.adapter.repository.user.UserRepository repository;

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users = repository.findAll();
        return users.stream()
                .map(user -> new UserDto(user.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<User> findUserById(String userId) {
        return repository.findById(userId);
    }

    @Override
    public void save(User user) {
        this.repository.save(user);
    }

    @Override
    public void update(User newUser) {

    }

    @Override
    public boolean exist(String userId) {
        return this.repository.existsById(userId);
    }

    @Override
    public void delete(String userId) {
        User user = findUserById(userId).get();
        this.repository.delete(user);
    }
}
