package co.crisi.hexagonal.infrastracture.adapter.repository.account;

import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.model.AccountId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, AccountId> {
    @Query("SELECT a FROM Account a WHERE a.userName = :userName AND a.domain = :domain")
    Optional<Account> findById(String userName, String domain);

    @Query("SELECT a FROM Account a WHERE a.userName = :userName")
    List<Account> findByUser(String userName);
}
