package co.crisi.hexagonal.infrastracture.adapter.repository.user;

import co.crisi.hexagonal.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
