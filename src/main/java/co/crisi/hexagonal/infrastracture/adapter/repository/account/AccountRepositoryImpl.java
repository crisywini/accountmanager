package co.crisi.hexagonal.infrastracture.adapter.repository.account;

import co.crisi.hexagonal.domain.dto.AccountDto;
import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.model.Category;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AccountRepositoryImpl implements AccountRepository {
    @Autowired
    private co.crisi.hexagonal.infrastracture.adapter.repository.account.AccountRepository accountRepository;
    @Autowired
    private co.crisi.hexagonal.infrastracture.adapter.repository.user.UserRepository userRepository;

    @Override
    public void save(String userName, String domain, String key, Category category) {
        User user = userRepository.findById(userName).get();
        Account account = new Account(userName, domain, key, category, user);
        accountRepository.save(account);
    }

    @Override
    public boolean exists(String userName, String domain) {
        return accountRepository.findById(userName, domain).isPresent();
    }

    @Override
    public void delete(String userName, String domain) {
        Account account = getById(userName, domain).get();
        accountRepository.delete(account);
    }

    @Override
    public Optional<Account> getById(String userName, String domain) {
        return accountRepository.findById(userName, domain);
    }

    @Override
    public List<Account> getByUser(String userName) {
        return accountRepository.findByUser(userName);
    }

    @Override
    public List<AccountDto> getAll() {
        List<Account> accounts = accountRepository.findAll();
        return accounts
                .stream()
                .map(account -> new AccountDto(account.getDomain(), account.getUserName()))
                .collect(Collectors.toList());
    }
}
