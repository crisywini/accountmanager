package co.crisi.hexagonal.domain.dto;

public class UserDto {
    private String userName;

    public UserDto(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
