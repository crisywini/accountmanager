package co.crisi.hexagonal.domain.dto;

public class AccountDto {
    private String domain, userName;

    public AccountDto(String domain, String userName) {
        this.domain = domain;
        this.userName = userName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
