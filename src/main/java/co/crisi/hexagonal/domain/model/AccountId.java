package co.crisi.hexagonal.domain.model;

import java.io.Serializable;
import java.util.Objects;

public class AccountId implements Serializable {

    private String userName;
    private String domain;

    public AccountId() {
        super();
    }

    public AccountId(String userName, String domain) {
        this.userName = userName;
        this.domain = domain;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountId accountId = (AccountId) o;
        return Objects.equals(userName, accountId.userName) && Objects.equals(domain, accountId.domain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, domain);
    }
}
