package co.crisi.hexagonal.domain.model;

import co.crisi.hexagonal.domain.model.validator.ArgumentValidator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Image implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "url", nullable = false)
    private String url;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "account_user_name", referencedColumnName = "userName"),
            @JoinColumn(name = "account_domain", referencedColumnName = "domain")
    })
    private Account account;


    private static final String REQUIRED_URL_MESSAGE = "The url is required";
    private static final String REQUIRED_ACCOUNT_MESSAGE = "The account is required";

    public Image() {
        super();
    }

    public Image(String url, Account account) {
        ArgumentValidator.verifyNotNull(url, REQUIRED_URL_MESSAGE);
        ArgumentValidator.verifyNotNull(account, REQUIRED_ACCOUNT_MESSAGE);
        this.url = url;
        this.account = account;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return id == image.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
