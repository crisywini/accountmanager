package co.crisi.hexagonal.domain.model;

import co.crisi.hexagonal.domain.model.validator.ArgumentValidator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@IdClass(AccountId.class)
public class Account implements Serializable {

    @Id
    @Column(name = "userName", nullable = false)
    private String userName;

    @Id
    @Column(name = "domain", nullable = false)
    private String domain;

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "category", nullable = false)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "accounter", referencedColumnName = "user_id")
    private User user;

    @OneToMany(mappedBy = "account")
    private List<Image> images;

    private static final String REQUIRED_USER_NAME_MESSAGE = "The user name is required";
    private static final String REQUIRED_KEY_MESSAGE = "The key is required";
    private static final String REQUIRED_CATEGORY_MESSAGE = "The category is required";
    private static final String REQUIRED_USER_MESSAGE = "The user is required";
    private static final String REQUIRED_DOMAIN_MESSAGE = "The domain is required";

    public Account() {
        super();
    }

    public Account(String userName, String domain, String key, Category category, User user) {
        ArgumentValidator.verifyNotNull(userName, REQUIRED_USER_NAME_MESSAGE);
        ArgumentValidator.verifyNotNull(key, REQUIRED_KEY_MESSAGE);
        ArgumentValidator.verifyNotNull(category, REQUIRED_CATEGORY_MESSAGE);
        ArgumentValidator.verifyNotNull(user, REQUIRED_USER_MESSAGE);
        ArgumentValidator.verifyNotNull(domain, REQUIRED_DOMAIN_MESSAGE);

        this.userName = userName;
        this.key = key;
        this.category = category;
        this.user = user;
        this.domain = domain;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "Account{" +
                "userName='" + userName + '\'' +
                ", domain='" + domain + '\'' +
                ", key='" + key + '\'' +
                ", category=" + category +
                ", user=" + user +
                ", images=" + images +
                '}';
    }
}
