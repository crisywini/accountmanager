package co.crisi.hexagonal.domain.model;

import co.crisi.hexagonal.domain.model.validator.ArgumentValidator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity(name = "accounter")
public class User implements Serializable {

    @Id
    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "password", nullable = false)
    private String password;

    @OneToMany(mappedBy = "user")
    private List<Account> accounts;

    private static final String REQUIRED_USER_MESSAGE = "The user is required";
    private static final String REQUIRED_PASSWORD_MESSAGE = "The password is required";

    public User() {
        super();
    }

    public User(String userId, String password) {
        ArgumentValidator.verifyNotNull(userId, REQUIRED_USER_MESSAGE);
        ArgumentValidator.verifyNotNull(password, REQUIRED_PASSWORD_MESSAGE);
        this.userId = userId;
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }
}
