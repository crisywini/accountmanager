package co.crisi.hexagonal.domain.model.validator;

import co.crisi.hexagonal.domain.exception.ArgumentNullException;

public class ArgumentValidator {

    public static void verifyNotNull(Object object, String errorMessage){
        if(object == null || object.toString().isEmpty()){
            throw new ArgumentNullException(errorMessage);
        }
    }
}
