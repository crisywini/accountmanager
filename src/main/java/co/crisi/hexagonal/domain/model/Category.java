package co.crisi.hexagonal.domain.model;

public enum Category {
    EDUCATIONAL,
    BANK,
    MEDIA;
}
