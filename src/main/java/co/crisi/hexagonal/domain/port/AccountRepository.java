package co.crisi.hexagonal.domain.port;

import co.crisi.hexagonal.domain.dto.AccountDto;
import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.model.Category;
import co.crisi.hexagonal.domain.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface AccountRepository {

    void save(String userName, String domain, String key, Category category);

    boolean exists(String userName, String domain);

    void delete(String userName, String domain);

    Optional<Account> getById(String userName, String domain);

    List<Account> getByUser(String userName);

    List<AccountDto> getAll();
}
