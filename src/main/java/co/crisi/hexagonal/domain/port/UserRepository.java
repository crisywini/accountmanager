package co.crisi.hexagonal.domain.port;

import co.crisi.hexagonal.domain.dto.UserDto;
import co.crisi.hexagonal.domain.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserRepository {
    List<UserDto> getAllUsers();

    Optional<User> findUserById(String userId);

    void save(User user);

    void update(User newUser);

    boolean exist(String userId);

    void delete(String userId);

}
