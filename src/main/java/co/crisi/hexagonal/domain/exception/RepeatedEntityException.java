package co.crisi.hexagonal.domain.exception;

public class RepeatedEntityException extends RuntimeException{
    public RepeatedEntityException(String s) {
        super(s);
    }
}
