package co.crisi.hexagonal.domain.exception;

public class ArgumentNullException extends RuntimeException{

    public ArgumentNullException(String s) {
        super(s);
    }
}
