package co.crisi.hexagonal.domain.exception;

public class EntityNullException extends RuntimeException{
    public EntityNullException(String s) {
        super(s);
    }
}
