package co.crisi.hexagonal.domain.service.account;

import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.port.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAccountsByUserService {
    private final AccountRepository accountRepository;

    public GetAccountsByUserService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> getByUser(String userName) {
        return accountRepository.getByUser(userName);
    }
}
