package co.crisi.hexagonal.domain.service.user;

import co.crisi.hexagonal.domain.exception.EntityNullException;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class DeleteUserService {

    private final UserRepository userRepository;
    private static final String NON_EXISTENCE_USER_MESSAGE = "The user does not exists!\n It cannot be removed";


    public DeleteUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void delete(String userId) {
        guaranteeExistenceUser(userId);
        userRepository.delete(userId);
    }

    private void guaranteeExistenceUser(String userId) {
        if (!userRepository.exist(userId)) {
            throw new EntityNullException(NON_EXISTENCE_USER_MESSAGE);
        }
    }
}
