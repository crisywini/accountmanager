package co.crisi.hexagonal.domain.service.user;

import co.crisi.hexagonal.domain.exception.EntityNullException;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class GetUserByIdService {

    private final UserRepository userRepository;
    private static final String NON_EXISTENCE_USER_MESSAGE = "The user does not exists";

    public GetUserByIdService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> get(String userName){
        verifyUserExistence(userName);
        return this.userRepository.findUserById(userName);
    }

    private void verifyUserExistence(String userName){
        if(!userRepository.exist(userName)){
            throw new EntityNullException(NON_EXISTENCE_USER_MESSAGE);
        }
    }

}
