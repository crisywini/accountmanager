package co.crisi.hexagonal.domain.service.account;

import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.port.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetAccountByIdService {

    private final AccountRepository accountRepository;

    public GetAccountByIdService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Optional<Account> getById(String userName, String domain) {
        return accountRepository.getById(userName, domain);
    }
}
