package co.crisi.hexagonal.domain.service.user;

import co.crisi.hexagonal.domain.dto.UserDto;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class GetAllUsersService {
    private final UserRepository userRepository;

    public GetAllUsersService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDto> getAll(){
        return this.userRepository.getAllUsers();
    }
}
