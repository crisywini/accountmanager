package co.crisi.hexagonal.domain.service.account;

import co.crisi.hexagonal.domain.exception.EntityNullException;
import co.crisi.hexagonal.domain.exception.RepeatedEntityException;
import co.crisi.hexagonal.domain.model.Category;
import co.crisi.hexagonal.domain.port.AccountRepository;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class SaveAccountService {
    private static final String EXISTENCE_ACCOUNT_MESSAGE = "The account already exists!";
    private static final String NON_EXISTENCE_USER_MESSAGE = "The user does not exists!";
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    public SaveAccountService(AccountRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    public void save(String userName, String domain, String key, Category category) {
        guaranteeNonExistenceAccount(userName, domain);
        guaranteeExistenceUser(userName);
        accountRepository.save(userName, domain, key, category);
    }

    private void guaranteeNonExistenceAccount(String userName, String domain) {
        if (accountRepository.exists(userName, domain)) {
            throw new RepeatedEntityException(EXISTENCE_ACCOUNT_MESSAGE);
        }
    }

    private void guaranteeExistenceUser(String userName) {
        if (!userRepository.exist(userName)) {
            throw new EntityNullException(NON_EXISTENCE_USER_MESSAGE);
        }
    }
}
