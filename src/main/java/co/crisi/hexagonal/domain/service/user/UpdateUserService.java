package co.crisi.hexagonal.domain.service.user;

import co.crisi.hexagonal.domain.exception.EntityNullException;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UpdateUserService {

    private final UserRepository userRepository;
    private static final String NON_EXISTENCE_USER_MESSAGE = "This user does not exists!";

    public UpdateUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void update(User newUser) {
        verifyUserExistence(newUser);
        this.userRepository.update(newUser);
    }

    private void verifyUserExistence(User user) {
        if (this.userRepository.exist(user.getUserId())) {
            throw new EntityNullException(NON_EXISTENCE_USER_MESSAGE);
        }
    }
}
