package co.crisi.hexagonal.domain.service.user;

import co.crisi.hexagonal.domain.exception.RepeatedEntityException;
import co.crisi.hexagonal.domain.model.User;
import co.crisi.hexagonal.domain.port.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class SaveUserService {

    private final UserRepository userRepository;
    private static final String REPEATED_USER_MESSAGE = "The user already exists!";

    public SaveUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(User user){
        verifyNotRepeatedUser(user);
        this.userRepository.save(user);
    }

    private void verifyNotRepeatedUser(User user){
        if(this.userRepository.exist(user.getUserId())){
            throw new RepeatedEntityException(REPEATED_USER_MESSAGE);
        }
    }

}
