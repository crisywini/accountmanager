package co.crisi.hexagonal.domain.service.account;

import co.crisi.hexagonal.domain.exception.EntityNullException;
import co.crisi.hexagonal.domain.port.AccountRepository;
import org.springframework.stereotype.Service;

@Service
public class DeleteAccountService {
    private static final String NON_EXISTENCE_ACCOUNT_MESSAGE = "The account does not exists!";
    private final AccountRepository accountRepository;

    public DeleteAccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void delete(String userName, String domain) {
        guaranteeExistenceAccount(userName, domain);
        accountRepository.delete(userName, domain);
    }

    private void guaranteeExistenceAccount(String userName, String domain) {
        if (!accountRepository.exists(userName, domain)) {
            throw new EntityNullException(NON_EXISTENCE_ACCOUNT_MESSAGE);
        }
    }
}
