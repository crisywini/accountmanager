package co.crisi.hexagonal.domain.service.account;

import co.crisi.hexagonal.domain.dto.AccountDto;
import co.crisi.hexagonal.domain.model.Account;
import co.crisi.hexagonal.domain.port.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllAccountsService {

    private final AccountRepository accountRepository;

    public GetAllAccountsService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<AccountDto> getAll() {
        return accountRepository.getAll();
    }
}
